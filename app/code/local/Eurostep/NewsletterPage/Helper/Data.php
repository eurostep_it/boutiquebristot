<?php

class Eurostep_NewsletterPage_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SECRET_KEY = '6Lf9JS4UAAAAADaRybIigdVYPB3NXnziVnKjvAHP';

    public function validateCaptcha($response)
    {
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . self::SECRET_KEY . '&response=' . $response);
        $responseData = json_decode($verifyResponse);
        if ($responseData->success):
            return true;
        else:
            return false;
        endif;
    }
}
	 