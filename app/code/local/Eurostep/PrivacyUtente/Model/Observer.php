<?php
class Eurostep_PrivacyUtente_Model_Observer {
    public function saveBillingobs(Varien_Event_Observer $observer) {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $postData = Mage::app()->getRequest()->getPost();

        $quote->setData('trattamento_dati_personali', $postData['billing']['trattamento_dati_personali']);
        $quote->setData('owner_utilizzo_dati', $postData['billing']['owner_utilizzo_dati']);
        $quote->setData('terze_parti_utilizzo_dati', $postData['billing']['terze_parti_utilizzo_dati']);

        if(isset($postData['customer_password']) && $postData['customer_password'] != ""):
            $logData = array(
                'Date' => date("d-m-Y H:i:s"),
                'Personal data consent' => (int)$postData['billing']['trattamento_dati_personali'],
                'Commercial data using by owner' => (int)$postData['billing']['owner_utilizzo_dati'],
                'Data using by third parts' => (int)$postData['billing']['terze_parti_utilizzo_dati']
            );

            Mage::log($logData, null, 'gdpr.log');
        endif;

        $quote->save();
    }

    /**
    * Utente registrato prima della GDPR aggiorna i dati al checkout
    **/
    public function updateCustomer(Varien_Event_Observer $observer) {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        if($quote->getData('customer_id') != null && $quote->getData('customer_id') != ""):
            $customer = $quote->getCustomer();

            $customer->setData('trattamento_dati_personali', (int)$quote->getData('trattamento_dati_personali'))->getResource()->saveAttribute($customer, 'trattamento_dati_personali');
            $customer->setData('owner_utilizzo_dati', (int)$quote->getData('owner_utilizzo_dati'))->getResource()->saveAttribute($customer, 'owner_utilizzo_dati');
            $customer->setData('terze_parti_utilizzo_dati', (int)$quote->getData('terze_parti_utilizzo_dati'))->getResource()->saveAttribute($customer, 'terze_parti_utilizzo_dati');

            $logData = array(
                'Date' => date("d-m-Y H:i:s"),
                'User ID' => $customer->getId(),
                'User email' => $customer->getEmail(),
                'Personal data consent' => (int)$customer->getData('trattamento_dati_personali'),
                'Commercial data using by owner' => (int)$customer->getData('owner_utilizzo_dati'),
                'Data using by third parts' => (int)$customer->getData('terze_parti_utilizzo_dati')
            );

            Mage::log($logData, null, 'gdpr.log');
        endif;
    }

    /**
    * Utente si iscrive/disiscrive alla newsletter e aggiorna il flag trattamento dati uso commerciale
    **/
    public function modificaUtente(Varien_Event_Observer $observer){
        $request = Mage::app()->getRequest();

        if($request->getActionName()!="formPost"):
            $postData = $request->getPost();
            $customer = $observer->getEvent()->getCustomer();

            if($request->getModuleName()=="newsletter"):
                $customer->setData('owner_utilizzo_dati', $postData['owner_utilizzo_dati'])->getResource()->saveAttribute($customer, 'owner_utilizzo_dati');

                if(isset($postData['terze_parti_utilizzo_dati'])){
                    $customer->setData('terze_parti_utilizzo_dati', $postData['terze_parti_utilizzo_dati'])->getResource()->saveAttribute($customer, 'terze_parti_utilizzo_dati');
                }
                $logData = array(
                    'Date' => date("d-m-Y H:i:s"),
                    'User ID' => $customer->getId(),
                    'User email' => $customer->getEmail(),
                    'Personal data consent' => (int)$customer->getData('trattamento_dati_personali'),
                    'Commercial data using by owner' => (int)$customer->getData('owner_utilizzo_dati'),
                    'Data using by third parts' => (int)$customer->getData('terze_parti_utilizzo_dati')
                );

                Mage::log($logData, null, 'gdpr.log');
            endif;
        endif;
    }

    public function logRegistration(Varien_Event_Observer $observer){
        $customer = $observer->getEvent()->getCustomer();

        $logData = array(
            'Date' => date("d-m-Y H:i:s"),
            'User ID' => $customer->getId(),
            'User email' => $customer->getEmail(),
            'Personal data consent' => (int)$customer->getData('trattamento_dati_personali'),
            'Commercial data using by owner' => (int)$customer->getData('owner_utilizzo_dati'),
            'Data using by third parts' => (int)$customer->getData('terze_parti_utilizzo_dati')
        );

        Mage::log($logData, null, 'gdpr.log');
    }
}