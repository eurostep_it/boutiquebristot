<?php
    $installer = $this;
    $installer->startSetup();

    $installer->addAttribute("customer", "trattamento_dati_personali",  array(
        "type"     => "int",
        "backend"  => "",
        "label"    => "Consenso trattamento dati personali",
        "input"    => "select",
        "source"   => "eav/entity_attribute_source_boolean",
        "visible"  => true,
        "required" => false,
        "default" => "",
        "frontend" => "",
        "unique"     => false,
        "note"       => ""
        )
    );

    $attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "trattamento_dati_personali");

    $used_in_forms=array();

    $used_in_forms[]="adminhtml_customer";
    $used_in_forms[]="checkout_register";
    $used_in_forms[]="customer_account_create";
    $used_in_forms[]="customer_account_edit";
    $used_in_forms[]="adminhtml_checkout";
    $attribute->setData("used_in_forms", $used_in_forms)
    ->setData("is_used_for_customer_segment", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 900);

    $attribute->save();

    $installer->addAttribute("customer", "owner_utilizzo_dati",  array(
        "type"     => "int",
        "backend"  => "",
        "label"    => "Utilizzo dati da parte del proprietario",
        "input"    => "select",
        "source"   => "eav/entity_attribute_source_boolean",
        "visible"  => true,
        "required" => false,
        "default" => "",
        "frontend" => "",
        "unique"     => false,
        "note"       => ""
        )
    );

    $attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "owner_utilizzo_dati");

    $used_in_forms=array();

    $used_in_forms[]="adminhtml_customer";
    $used_in_forms[]="checkout_register";
    $used_in_forms[]="customer_account_create";
    $used_in_forms[]="customer_account_edit";
    $used_in_forms[]="adminhtml_checkout";
    $attribute->setData("used_in_forms", $used_in_forms)
    ->setData("is_used_for_customer_segment", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 901);

    $attribute->save();

    $installer->addAttribute("customer", "terze_parti_utilizzo_dati",  array(
        "type"     => "int",
        "backend"  => "",
        "label"    => "Utilizzo dati da parte di aziende terze",
        "input"    => "select",
        "source"   => "eav/entity_attribute_source_boolean",
        "visible"  => true,
        "required" => false,
        "default" => "",
        "frontend" => "",
        "unique"     => false,
        "note"       => ""
        )
    );

    $attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "terze_parti_utilizzo_dati");

    $used_in_forms=array();

    $used_in_forms[]="adminhtml_customer";
    $used_in_forms[]="checkout_register";
    $used_in_forms[]="customer_account_create";
    $used_in_forms[]="customer_account_edit";
    $used_in_forms[]="adminhtml_checkout";
    $attribute->setData("used_in_forms", $used_in_forms)
    ->setData("is_used_for_customer_segment", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 902);

    $attribute->save();

    $installer->endSetup();