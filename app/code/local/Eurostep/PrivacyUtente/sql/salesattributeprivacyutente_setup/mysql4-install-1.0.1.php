<?php
    $installer = $this;
    $installer->startSetup();

    $installer->addAttribute("quote", "trattamento_dati_personali", array("type"=>"int"));
    $installer->addAttribute("quote", "owner_utilizzo_dati", array("type"=>"int"));
    $installer->addAttribute("quote", "terze_parti_utilizzo_dati", array("type"=>"int"));
    $installer->endSetup();