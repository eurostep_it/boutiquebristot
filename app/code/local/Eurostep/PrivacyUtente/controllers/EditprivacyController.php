<?php
    class Eurostep_PrivacyUtente_EditprivacyController extends Mage_Core_Controller_Front_Action {
        /**
        * Utente modifica i 3 flag privacy dall'apposita area utente
        **/
        public function editprivacyAction() {
            $postData = $this->getRequest()->getParams();

            $customer = Mage::helper('customer')->getCustomer();

            $customer->setData('trattamento_dati_personali', $postData['trattamento_dati_personali'])->getResource()->saveAttribute($customer, 'trattamento_dati_personali');
            $customer->setData('owner_utilizzo_dati', $postData['owner_utilizzo_dati'])->getResource()->saveAttribute($customer, 'owner_utilizzo_dati');
            $customer->setData('terze_parti_utilizzo_dati', $postData['terze_parti_utilizzo_dati'])->getResource()->saveAttribute($customer, 'terze_parti_utilizzo_dati');

            $logData = array(
                'Date' => date("d-m-Y H:i:s"),
                'User ID' => $customer->getId(),
                'User email' => $customer->getEmail(),
                'Personal data consent' => (int)$customer->getData('trattamento_dati_personali'),
                'Commercial data using by owner' => (int)$customer->getData('owner_utilizzo_dati'),
                'Data using by third parts' => (int)$customer->getData('terze_parti_utilizzo_dati')
            );

            Mage::log($logData, null, 'gdpr.log');

            $url = Mage::getUrl('gdpr/customer/settings');
            Mage::app()->getFrontController()->getResponse()->setRedirect($url)->sendResponse();
            $message = Mage::helper('customer')->__('The account information has been saved.');
            Mage::getSingleton('core/session')->addSuccess($message);
        }
    }